﻿module FsFhtw.Tests

open Xunit
open Domain

let song1 = { Song.name = "Song1" }
let song2 = { Song.name = "Song2" }

[<Fact>]
let ``That initial state is Idle`` () =
    let initialState = init ()

    let expected = State.Idle

    Assert.Equal(expected, initialState.state)

[<Fact>]
let ``That adding a song to the queue adds it to the front of the queue`` () =

    let initQueue = [ song1 ]

    let initialState =
        { state = Idle
          queue = { items = initQueue }
          history = { items = [] } }

    let actual = initialState |> update (AddToQueue song2)

    let expected =
        { state = Idle
          queue = { items = [ song1; song2 ] }
          history = { items = [] } }

    Assert.Equal(expected, actual)


[<Fact>]
let ``That playing a song plays it`` () =

    let initQueue = [ song1 ]

    let initialState =
        { state = Idle
          queue = { Queue.items = initQueue }
          history = { items = [] } }

    let actual = initialState |> update Play

    let expected =
        { state = Playing song1
          queue = { items = [] }
          history = { items = [] } }

    Assert.Equal(expected, actual)

[<Fact>]
let ``That pausing a song pauses it`` () =

    let initQueue = [ song2 ]

    let initialState =
        { state = Playing song1
          queue = { items = initQueue }
          history = { items = [] } }

    let actual = initialState |> update Pause

    let expected =
        { state = Paused song1
          queue = { items = initQueue }
          history = { items = [] } }

    Assert.Equal(expected, actual)

[<Fact>]
let ``That skipping to the next song plays it`` () =

    let initQueue = [ song1; song2 ]

    let initialState =
        { state = Idle
          queue = { items = initQueue }
          history = { items = [] } }

    let actual = initialState |> update Next

    let expected =
        { state = Playing song1
          queue = { items = [ song2 ] }
          history = { items = [] } }

    Assert.Equal(expected, actual)
    
[<Fact>]
let ``That skipping to the next song adds current song to history`` () =

    let initQueue = [ song2 ]

    let initialState =
        { state = Playing song1
          queue = { items = initQueue }
          history = { items = [] } }

    let actual = initialState |> update Next

    let expected =
        { state = Playing song2
          queue = { items = [] }
          history = { items = [ song1 ] } }

    Assert.Equal(expected, actual)
    
[<Fact>]
let ``That switching to previous song plays it and adds current song to queue`` () =

    let initQueue = []
    let initHistory = [ song1 ]

    let initialState =
        { state = Playing song2
          queue = { items = initQueue }
          history = { items = initHistory } }

    let actual = initialState |> update Previous

    let expected =
        { state = Playing song1
          queue = { items = [ song2 ] }
          history = { items = [] } }

    Assert.Equal(expected, actual)

[<Fact>]
let ``That showing the queue does not change it`` () =

    let initQueue = [ song1; song2 ]

    let initialState =
        { state = Idle
          queue = { Queue.items = initQueue }
          history = { items = [] } }

    let actual = initialState |> update ShowQueue

    let expected = initialState

    Assert.Equal(expected, actual)
