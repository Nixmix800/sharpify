module Domain
open System

type Song =
    {
        name : string
    }

type State =
    | Idle
    | Playing of Song
    | Paused of Song

type Queue =
    {
        items : list<Song>
    }

type Player =
    {
        state : State
        queue : Queue
        history : Queue
    }

type Message =
    | Play
    | Pause
    | Next
    | Previous
    | AddToQueue of Song
    | ShowQueue
    | ShowHistory
    | Shuffle

let init () : Player =
    {
        state = Idle
        queue = {items = []}
        history = {items = []}
    }

let popSongFromQueue (queue : Queue) : (Option<Song> * Queue) =
    match queue.items with
    | [] -> (None, queue)
    | head :: tail -> (Some head, { items = tail })

let getCurrentSong (player : Player) : Option<Song> =
    match player.state with
    | State.Playing song
    | State.Paused song ->
        Some song
    | _ ->
        None

let addSongToQueue (queue : Queue) (optionSong : Option<Song>) : Queue =
    match optionSong with
    | Some song ->
        { items = [ song ] @ queue.items}
    | None ->
        queue

let transferBetweenQueues (song : Option<Song>) (firstQueue : Queue) (secondQueue : Queue) : (Option<Song> * Queue * Queue) =
    let newSong, queue1 = popSongFromQueue firstQueue
    let queue2 = addSongToQueue secondQueue song
    (newSong, queue1, queue2)

let playNextSongInQueue (player : Player) : Player =
    let currentSong = getCurrentSong player
    let newSong, queue, history = transferBetweenQueues currentSong player.queue player.history
    match newSong with
    | Some song ->
        {
            state = State.Playing song
            queue = queue
            history = history
        }
    | None ->
        {
            state = State.Idle
            queue = queue
            history = history
        }

let playPreviousSongFromHistory (player : Player) : Player =
    let currentSong = getCurrentSong player
    let newSong, history, queue = transferBetweenQueues currentSong player.history player.queue
    match newSong with
    | Some song ->
        {
            state = State.Playing song
            queue = queue
            history = history
        }
    | None ->
        {
            state = State.Idle
            queue = queue
            history = history
        }

let resumePlaying (model: Player) : Player =
    match model with
    | { Player.state = Paused song; Player.queue = queue; Player.history = history} ->
        {
            state = State.Playing song;
            queue = queue
            history = history
        }
    | _ -> model

let handlePause (model: Player) : Player =
    match model with
    | { Player.state = Playing song; Player.queue = queue; Player.history = history} ->
        {
            state = State.Paused song
            queue = queue
            history = history
        }
    | _ -> model

let handlePlay (model : Player) : Player =
    match model.state with
    | Idle -> playNextSongInQueue model
    | Playing _ -> model
    | Paused _ -> resumePlaying model

let handleNext (model : Player) : Player =
    playNextSongInQueue model

let handlePrevious (model : Player) : Player =
    playPreviousSongFromHistory model

let handleAddToQueue (model : Player) (song : Song) : Player =
    {
        state = model.state
        queue = { items = model.queue.items @ [ song ] }
        history = model.history
    }

let shuffle (queue : Queue) =
    let rnd = Random ()
    {items = queue.items |> List.sortBy(fun _ -> rnd.Next(1, queue.items.Length * 10) )}

let handleShuffle (model : Player) : Player =
    {
        state = model.state
        queue = shuffle model.queue
        history = model.history
    }

let update (message : Message) (model : Player) : Player =
    match message with
    | Play -> handlePlay model
    | Pause -> handlePause model
    | Next -> handleNext model
    | Previous -> handlePrevious model
    | Shuffle -> handleShuffle model
    | AddToQueue song -> handleAddToQueue model song
    | ShowQueue -> model
    | ShowHistory -> model
