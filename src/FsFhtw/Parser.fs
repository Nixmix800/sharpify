module Parser

open System

let safeEquals (it : string) (theOther : string) =
    String.Equals(it, theOther, StringComparison.OrdinalIgnoreCase)

type Message =
    | Play
    | Pause
    | Next
    | Previous
    | AddToQueue of string
    | ShowQueue
    | ShowHistory
    | Shuffle
    | Help
    | ParseFailed

[<Literal>]
let HelpLabel = "Help"

let parse (input : string) : Message =

    let parts = input.Split(' ') |> List.ofArray
    match parts with
    | [ verb ] when safeEquals verb (nameof Domain.Play) -> Play
    | [ verb ] when safeEquals verb (nameof Domain.Pause) -> Pause

    | [ verb ] when safeEquals verb (nameof Domain.Next) -> Next
    | [ verb ] when safeEquals verb (nameof Domain.Previous) -> Previous

    | [ verb ] when safeEquals verb (nameof Domain.ShowQueue) -> ShowQueue
    | [ verb ] when safeEquals verb ("Show") -> ShowQueue

    | [ verb ] when safeEquals verb (nameof Domain.ShowHistory) -> ShowHistory
    | [ verb ] when safeEquals verb ("History") -> ShowHistory

    | [ verb ] when safeEquals verb (nameof Domain.Shuffle) -> Shuffle

    | [ verb; arg ] when safeEquals verb (nameof Domain.AddToQueue) ->
        AddToQueue arg
    | [ verb; arg ] when safeEquals verb ("Add") -> //AddToQueue abbreviation for easier usage
        AddToQueue arg

    | [ verb ] when safeEquals verb HelpLabel -> Help
    | _ -> ParseFailed
