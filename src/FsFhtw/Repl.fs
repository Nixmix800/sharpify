module Repl

open System
open Domain
open Parser

type Message =
    | DomainMessage of Domain.Message
    | HelpRequested
    | NotParsable of string

type Player = Domain.Player
type State = Domain.State
let read (input : string) =
    match parse input with
    | Play -> Domain.Play |> DomainMessage
    | Pause -> Domain.Pause |> DomainMessage
    | Next -> Domain.Next |> DomainMessage
    | Previous -> Domain.Previous |> DomainMessage
    | AddToQueue song -> Domain.AddToQueue {name = song} |> DomainMessage
    | ShowQueue -> Domain.ShowQueue |> DomainMessage
    | ShowHistory -> Domain.ShowHistory |> DomainMessage
    | Shuffle -> Domain.Shuffle |> DomainMessage
    | Help -> HelpRequested
    | ParseFailed  -> NotParsable input


open Microsoft.FSharp.Reflection

let createHelpText () : string =
    FSharpType.GetUnionCases typeof<Domain.Message>
    |> Array.map (fun case -> case.Name)
    |> Array.fold (fun prev curr -> prev + " " + curr) ""
    |> (fun s -> s.Trim() |> sprintf "Known commands are: %s")

let createPlayerText (player : Player) : string =
    match player.state with
    | State.Playing song ->
        $"Playing {song} ({(List.length player.queue.items)} songs left in queue)."
    | State.Paused song -> $"Pausing {song}"
    | _ -> "Playing nothing."

let createAddToQueueText (player : Player) (song : Song) : string =
    $"{song.name} successfully added to queue."

let evaluate (update : Domain.Message -> Player -> Player) (player : Player) (msg : Message) =
    match msg with
    | DomainMessage msg ->
        let newPlayer = update msg player;
        let message =
            match msg with
                | Domain.Message.ShowQueue ->
                    String.concat "\n" [$"Queue is:\n {newPlayer.queue}"; (createPlayerText player)]
                | Domain.Message.ShowHistory ->
                    String.concat "\n" [$"History is:\n {newPlayer.history}"; (createPlayerText player)]
                | Domain.Message.AddToQueue song ->
                    String.concat "\n" [(createAddToQueueText newPlayer song); (createPlayerText player)]
                | _ ->
                    createPlayerText newPlayer
        (newPlayer, message)
    | HelpRequested ->
        let message = createHelpText ()
        (player, message)
    | NotParsable originalInput ->
        let message =
            sprintf """"%s" was not parsable. %s"""  originalInput "You can get information about known commands by typing \"Help\""
        (player, message)

let print (player : Player, outputToPrint : string) =
    printfn "%s\n" outputToPrint
    printf "> "

    player

let rec loop (state : Player) =
    Console.ReadLine()
    |> read
    |> evaluate Domain.update state
    |> print
    |> loop
